---
- hosts: all
  vars_files:
    - ./dotfiles_vars.yml
  vars:
    repository_directory_local: "{{ temp_local }}/repository"
    repository_directory_remote: "{{ temp_remote }}/repository"

  tasks:
    - name: Create remote directories
      file:
        path: "{{ item }}"
        state: directory
      with_items:
        - "{{ dotfiles_home_remote }}"
        - "{{ temp_remote }}"
      when: operation_mode == "install"

    - name: Create local directories
      file:
        path: "{{ item }}"
        state: directory
      with_items:
        - "{{ dotfiles_home_local }}"
        - "{{ temp_local }}"
      when: operation_mode == "download"

    - name: Copy repository directory
      synchronize:
        src: "{{ repository_directory_local }}/"
        dest: "{{ repository_directory_remote }}"
      when: operation_mode == "install"

    - name: Install stow
      apt:
        name: stow
        state: latest
      become: true

    - include_role:
        name: common
      when: '"common" in executed_roles'

    - include_role:
        name: fzf
      when: '"fzf" in executed_roles'

    - include_role:
        name: vim
      when: '"vim" in executed_roles'

    - include_role:
        name: samba
      when: '"samba" in executed_roles'

    - name: Remove remote temp directory
      file:
        path: "{{ temp_remote }}"
        state: absent
      when: operation_mode == "install"

    - name: Remove local directories
      file:
        path: "{{ item }}"
        state: absent
      with_items:
        - "{{ temp_local }}"
      when: operation_mode == "clean"

    - name: Remove remote dotfiles directory
      file:
        path: "{{ dotfiles_home_remote }}"
        state: absent
      when: operation_mode == "remove"

    - name: Remove broken symbolic links
      command: find -xtype l -delete
      args:
        chdir: "{{ ansible_env['HOME'] }}"
      become: true
      when: operation_mode == "remove"
