# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [1.5.0]
### Added
- Add auto push annotated tags in git
- Add auto updating git subumodules at checkout
- Add plugin to fix indentation in python file type
- Better syntax highlighting for python in vim vim semshi plugin

### Changed
- Update bat vesion
- Update fzf version
- Update all vim plugins
- Install neovim from apt repository instead of AppImage
- Disable stripping whitespace on save

### Removed
- Annoying paste configuration
- Outdated deoplete configuration
- vim goyo plugin
- vim ansible plugin

### Fixed
- Wrong LSP client configuration causing error

## [1.4.1]
### Added
- Can execute command only in specific roles: "config.sh install vim"
- Add pynvim package, just in case
- Add chromatica.nvim plugin

### Changed
- Update submodules
- Auto install stow without waiting to user input
- Improved format of git glog to show branch names, time and author in the end
- Update neovim binary version
- Hidden buffers in vim

### Removed
- Remove F12 keybinding from tmux configuration, not sure what it did
- Remove unused i3 configuration

### Fixed
- Fix hover errors of language client in neovim caused by update


## [1.4.0]
### Added
- Implement installing deb packages offline
- Add installing required packages for pip_offline
- Add stopping the smbd and nmbd services when removing samba role
- Install ansible and stow if needed in config.sh script
- Install pip2pi if missing on download operation
- Add visual mode mapping for easymotion

### Changed
- Change vim clipboard to system clipboard
- Update all submodules
- Disable indent line plugin for vim when the file type is json

### Removed
- Remove python 2 packages
- Remove unused function in config.sh script


## [1.3.0]
### Added
- Git:
  - Add "in" and "out" git aliases that works like mercurial's "in" and "out"
  - git rerere is now enabled by default
  - Add git branch and current repository status to bash prompt
  - Add alias "rerere-reset" to clean rerere's cache of recorded resolutions
- Vim:
  - Buffer are auto updated if files are changed on disk
  - Add installing C language client
  - Add command aliases for :w and :wq to make them practically case-insensitive
  - Add auto windows resizing if vim is resized

### Changed
- Move installing, downloading, cleaning and removing vim related python packages
  to the vim role
- Git:
  - "co" alias now contains the "--recurse-submodules" flag
- Common:
  - tmux key bindings for new window, vertical split, horizontal split and
    full screen are changed to ALT-c, ALT-v, ALT-s and ALT-f, respectively
- Vim:
  - LanguagecClient-neovim is updated to latest version
  - Only modified lines are now stripped to improve the readability of merges

### Removed
- Remove vim "zb" and "zt" key bindings to leave margin of 3 rows from top/bottom

### Fixed
- Help files in vim are now displayed without intentation vertical lines


## [1.2.1]
### Fixed
- Error in vim startup, causing installation to hang on :UpdateRemotePlugin
- Syntax error in uninstall operation in common role related to removing binaries 


## [1.2.0]
### Added
- Clean action to remove content downloaded by the download action
- Almost complete uninstallation of the configurations
- Git:
  - Forward-branch alias
  - Use vimdiff to solve conflicts
- Common:
  - Add exa executable as drop-in replacement to ls with bash aliases
  - Bash history configurations
- Vim:
  - Add calling :UpdateRemotePlugin as ansible task

### Changed
- Major refactoring:
  - Split the main playbook into roles
  - Choosing action is done by global operation_mode variable and task includes
  - Every role copy its own file - no global copying
  - Samba is now installed by the main playbook
  - Repeated operation is now implemented with action plugins
    - stow
    - pip download offline and install
- Common:
  - Move to use .bash_profile instead of .bashrc
  - Bash configuration is now hosted in .config/dotfiles/bash
- Vim:
  - Install vim executable to system wide path

### Removed
- Unused inventory file

### Fixed
- Common:
  - Fix showing stderr in cdf bash alias
- Vim:
  - Fix error at startup if no C language server is present
- Every role and action can now be executed multiple times
- Fix multiple small typos

## [1.1.0]
### Added
- i3 configuration
- Generate completion scripts for FZF
- Download LSP client for vim
- Symbolic link to pathogen plugin
- Ansible plugin to vim

### Changed
- Download python packages and installing the seperately
- Refactor the main playbook

### Removed
- Unused aliases

### Fixed
- Set permissions of files in .local/bin, languageclient and fzf
- Typos in tasks names in the main playbook
- Install acl for ansible module in samba playbook
- Uninstall commands can be run multiple times
- Bug creating file named '1' when using config.sh
- Bug in init.vim that causes the status line of tmux to disappear when using
  goyo in vim
- Paths of submodules


## [1.0.0]
### Added
- Main ansible playbook to automatically deploy the configurations
- Wrapper config.sh script for easy deployment
- Vim configuration with plugins
- Bash config and bash aliases
- Git configuration
- Readline configuration
- Tmux configuration
- FZF installation
- Samba configuration with ansible playbook
- GLWTS License

