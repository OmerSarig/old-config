if [ -f "$HOME/.profile" ]; then
    . "$HOME/.profile"
fi

for filename in "${XDG_CONFIG_HOME:-$HOME/.config}"/bash/*; do
    source $filename
done
