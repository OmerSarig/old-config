wg_is_keys_off="#[fg=$color_light,bg=$color_window_off_indicator]#([ $(tmux show-option -qv key-table) = 'off' ] && echo 'OFF')#[default]"

set -g status-right "$wg_is_keys_off #{sysstat_cpu} | #{sysstat_mem} | #{sysstat_loadavg} | $wg_user_host"

# vim tmux integration
is_vim="ps -o state= -o command= -t '#{pane_tty}' \
    | grep -iqE '^[^TXZ ]+ +(\\S+\\/)?g?(view|n?vim?x?)(diff)?$'"
bind-key -n M-h if-shell "$is_vim" "send-keys M-h"  "select-pane -L"
bind-key -n M-j if-shell "$is_vim" "send-keys M-j"  "select-pane -D"
bind-key -n M-k if-shell "$is_vim" "send-keys M-k"  "select-pane -U"
bind-key -n M-l if-shell "$is_vim" "send-keys M-l"  "select-pane -R"
bind-key -n M-\ if-shell "$is_vim" "send-keys M-\\" "select-pane -l"
bind-key -T copy-mode-vi M-h select-pane -L
bind-key -T copy-mode-vi M-j select-pane -D
bind-key -T copy-mode-vi M-k select-pane -U
bind-key -T copy-mode-vi M-l select-pane -R
bind-key -T copy-mode-vi M-\ select-pane -l

bind-key -n M-f resize-pane -Z

# windows navigating
bind-key -n M-L next-window
bind-key -n M-H previous-window

# set vim-like keys
setw -g mode-keys vi
set-window-option -g xterm-keys on
set-window-option -g mode-keys vi
set-option -g status-keys vi

# set numbering
set -g base-index 1
setw -g pane-base-index 1

# set naming
setw -g automatic-rename on
set -g renumber-windows on
set -g set-titles on

# disable escape sleep
set -sg escape-time 0

# enable 256 colors support
set -g default-terminal "xterm-256color"

# Create windows and panes from current directory
bind-key -n M-c new-window -c "#{pane_current_path}"
bind-key -n M-v split-window -h -c "#{pane_current_path}"
bind-key -n M-s split-window -v -c "#{pane_current_path}"

set -g focus-events on
