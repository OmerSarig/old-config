#!/bin/bash

install-if-needed() {
    if ! hash $2 2>/dev/null; then
        $1 $2
    fi
}

convert_to_json_list() {
    echo "[\"$(echo "$*" | sed 's/ /", "/g')\"]"
}

main() {
    local operation_mode=$1
    shift 1
    if [ "$operation_mode" = "install" ] || [ "$operation_mode" = "remove" ]; then
        local target=$1
        shift 1
    fi

    if [ $# -gt 0 ]; then
        local roles=$(convert_to_json_list $*)
    else
        local roles=$(convert_to_json_list common vim fzf samba)
    fi

    local extra_vars="{'operation_mode': '$operation_mode', 'executed_roles': $roles}"

    install-if-needed "sudo pip3 install" ansible
    install-if-needed "sudo apt install -y" stow

    export ANSIBLE_NOCOWS=1
    export ANSIBLE_DISPLAY_SKIPPED_HOSTS=0

    case "$operation_mode" in
        install)
            ansible-playbook config.yml -e "$extra_vars" -i $target, --ask-become-pass
            ;;
        remove)
            ansible-playbook config.yml -e "$extra_vars" -i $target, --ask-become-pass
            ;;
        download)
            install-if-needed "sudo pip3 install" pip2pi
            ansible-playbook config.yml -e "$extra_vars" -i 127.0.0.1, -c local
            ;;
        clean)
            ansible-playbook config.yml -e "$extra_vars" -i 127.0.0.1, -c local
            ;;
        *)
            echo "Usage: $0 install|remove|download|clean"
            ;;
    esac
}

main $*
