"""
This module implements pip offline
actions action plugin for ansible playbooks
"""
from ansible.plugins.action import ActionBase

DOWNLOAD_ACTION = 'download'
INSTALL_ACTION = 'install'


class ActionModule(ActionBase):
    def run(self, tmp=None, task_vars=None):
        args = self._task.args
        action = args['action']
        requirements = args['requirements']
        directory = args['directory']
        if 'executable' in args:
            executable = args['executable']
        else:
            executable = 'pip'

        if action == DOWNLOAD_ACTION:
            return_value = self._execute_module(
                module_name='command',
                module_args={'argv': ['pip2pi', directory, '--prefer-binary', 'wheel', '-r', requirements]},
                task_vars=task_vars
            )
        elif action == INSTALL_ACTION:
            return_value = self._execute_module(
                module_name='pip',
                module_args={
                    'requirements': requirements,
                    'extra_args': '--no-index --find-links={}'.format(directory),
                    'executable': executable
                },
                task_vars=task_vars
            )
        else:
            return_value = {}

        return return_value
