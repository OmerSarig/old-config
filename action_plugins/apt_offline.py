"""
This module implements apt offline repository
action plugin for ansible playbooks
"""
import re

from ansible.plugins.action import ActionBase
from ansible.errors import AnsibleError
from ansible.module_utils._text import to_native

DOWNLOAD_ACTION = 'download'
INSTALL_ACTION = 'install'
PACKAGE_REGEX = re.compile(r'^\w')


def _raise_low_level_command_exception(message, command_result, **kwargs):
    """
    This is temporary way to handle errors because ansible doesn't have
    sane error handling for action plugins
    """
    if command_result['rc'] != 0:
        raise AnsibleError(
            message.format(
                stderr=command_result['stderr'],
                stdout=command_result['stdout'],
                **kwargs
            ),
        )


class ActionModule(ActionBase):

    def get_package_dependencies(self, packages):
        """
        Return a list of the dependencies of list of packages.
        If multiple packages shares the same dependencies, it won't be duplicated
        """
        argv = [
            "apt-cache",
            "depends",
            "--recurse",
            "--no-recommends",
            "--no-suggests",
            "--no-conflicts",
            "--no-breaks",
            "--no-replaces",
            "--no-enhances",
        ]
        argv.extend(packages)
        command_result = self._low_level_execute_command(' '.join(argv))
        _raise_low_level_command_exception(
            'Failed to determine packages dependencies: {stderr}\n{stdout}',
            command_result
        )

        packages = {
            line for line in command_result['stdout_lines']
            if PACKAGE_REGEX.match(line) is not None
        }
        return packages

    def download_packages(self, packages, directory):
        """ Downloads list of packages to a given directory """
        module_result = self._execute_module(
            module_name='file',
            module_args={'path': directory, 'state': 'directory'}
        )
        if module_result.get('failed', False):
            raise AnsibleError('Failed to create directory {directory}'.format(directory=directory))

        argv = ['apt-get', 'download']
        argv.extend(packages)
        command_result = self._low_level_execute_command(' '.join(argv), chdir=directory)
        _raise_low_level_command_exception('Failed to download packages: {stderr}\n{stdout}', command_result)

    def generate_packages_file(self, directory_local):
        """ Generate 'Packages.gz' file required to setup a local repository """
        # Package dpkg-dev is required for this to work
        argv = ['dpkg-scanpackages', '-m', '.', '/dev/null', '>', 'Packages']
        command_result = self._low_level_execute_command(' '.join(argv), chdir=directory_local)
        _raise_low_level_command_exception('Failed to generate Packages.gz file: {stderr}\n{stdout}', command_result)

    def generate_sources_list_file(self, directory_remote):
        """ Create sources.list files required to the repository """
        sources_list_file_name = 'sources.list'
        argv = ['echo', 'deb', '[trusted=yes]', 'file://' + directory_remote, './', '>', sources_list_file_name]
        command_result = self._low_level_execute_command(' '.join(argv), chdir=directory_remote)
        _raise_low_level_command_exception(
            'Failed to generate {sources_list} file: {stderr}\n{stdout}',
            command_result,
            sources_list=sources_list_file_name
        )

    def update_and_install_packages(self, directory_remote, package_list):
        """
        Update the packages list cache with the local repository
        and install the given packages.
        """
        sources_list_option = [
            '-o',
            'Dir::Etc::SourceList=./sources.list',
        ]
        argv = [
            'apt-get',
            'update',
            '-o',
            'APT::Get::List-Cleanup="0"'
        ]
        argv.extend(sources_list_option)
        command_result = self._low_level_execute_command(' '.join(argv), chdir=directory_remote)
        _raise_low_level_command_exception('Failed to install packages: {stderr}\n{stdout}', command_result)

        argv = ['apt-get', 'install', '-y']
        argv.extend(sources_list_option)
        argv.extend(package_list)
        command_result = self._low_level_execute_command(' '.join(argv), chdir=directory_remote)
        _raise_low_level_command_exception('Failed to install packages: {stderr}\n{stdout}', command_result)

    def run(self, tmp=None, task_vars=None):
        return_value = {}

        args = self._task.args.copy()
        action = args['action']
        packages = args['packages']
        directory_local = args['directory_local']

        if action == DOWNLOAD_ACTION:
            package_list = self.get_package_dependencies(packages)
            self.download_packages(package_list, directory_local)
            self.generate_packages_file(directory_local)
        elif action == INSTALL_ACTION:
            directory_remote = args['directory_remote']
            self.generate_sources_list_file(directory_remote)
            self.update_and_install_packages(directory_remote, packages)

        return return_value
