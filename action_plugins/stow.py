""" This module implements stow action plugin for ansible playbooks """
from ansible.plugins.action import ActionBase


ACTION_ARGS = {
    'install': '-S',
    'remove': '-D',
    'reinstall': '-R'
}


class ActionModule(ActionBase):
    def run(self, tmp=None, task_vars=None):
        args = self._task.args
        if 'action' in args:
            action = args['action']
        else:
            action = ACTION_ARGS['install']
        stow_dir = args['stow_dir']
        target = args['target']
        package = args['package']

        command_module_args = [
            'stow',
            ACTION_ARGS[action],
            '--dir={}'.format(stow_dir),
            '--target={}'.format(target),
            package
        ]
        if 'defer' in args:
            command_module_args.append('--defer={}'.format(args['defer']))

        module_return = self._execute_module(
            module_name='command',
            module_args={'argv': command_module_args},
            task_vars=task_vars
        )
        return module_return
